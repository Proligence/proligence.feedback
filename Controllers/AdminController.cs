using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;
using Orchard;
using Orchard.DisplayManagement;
using Orchard.Localization;
using Orchard.Settings;
using Orchard.UI.Navigation;
using Proligence.Feedback.Services;
using Proligence.Feedback.ViewModels;

namespace Proligence.Feedback.Controllers  {
    public class AdminController : Controller  {
        private readonly IIssueTracker _issueTracker;
        private readonly ISiteService _siteService;

        public IOrchardServices Services { get; set; }
        private dynamic Shape { get; set; }

        public AdminController(
            IOrchardServices services,
            IIssueTracker issueTracker,
            ISiteService siteService,
            IShapeFactory shapeFactory) {
            _issueTracker = issueTracker;
            _siteService = siteService;
            Services = services;
            T = NullLocalizer.Instance;
            Shape = shapeFactory;
        }

        public Localizer T { get; set; }

        public ActionResult Index(FeedbackIssueIndexOptions options, PagerParameters pagerParameters)  {
            if (!Services.Authorizer.Authorize(Permissions.ManageFeedback, T("Not authorized to list feedback issues"))) {
                return new HttpUnauthorizedResult();
            }

            var pager = new Pager(_siteService.GetSiteSettings(), pagerParameters);

            // default options
            if (options == null) {
                options = new FeedbackIssueIndexOptions();
            }

            var filter = "";
            switch (options.Filter) {
                case IssuesFilter.New:
                    filter += "+state:+New";
                    break;
                case IssuesFilter.Open:
                    filter += "+state:+Open";
                    break;
                case IssuesFilter.ToBeDiscussed:
                    filter += "+state:+{To+Be+Discussed}";
                    break;
                case IssuesFilter.InProgress:
                    filter += "+state:+{In+Progress}";
                    break;
                case IssuesFilter.CantReproduce:
                    filter += "+state:+{Can't+Reproduce}";
                    break;
                case IssuesFilter.Duplicate:
                    filter += "+state:+Duplicate";
                    break;
                case IssuesFilter.WontFix:
                    filter += "+state:+{Won't+Fix}";
                    break;
                case IssuesFilter.Obsolete:
                    filter += "+state:+Obsolete";
                    break;
                case IssuesFilter.Closed:
                    filter += "+state:+Closed";
                    break;
                case IssuesFilter.WaitingVerification:
                    filter += "+state:+{Waiting+Verification}";
                    break;
            }

            if (string.IsNullOrWhiteSpace(options.Search) == false) {
                filter = string.IsNullOrWhiteSpace(filter)
                    ? options.Search
                    : string.Format("{0}+{1}", filter, options.Search);
            }

            var sortExpression = "sort+by:+";
            switch (options.Order) {
                case IssuesOrder.IdAsc:
                    sortExpression += "{issue+id}+asc";
                    break;
                case IssuesOrder.IdDesc:
                    sortExpression += "{issue+id}+desc";
                    break;
                case IssuesOrder.StateAsc:
                    sortExpression += "State+asc";
                    break;
                case IssuesOrder.StateDesc:
                    sortExpression += "State+desc";
                    break;
                case IssuesOrder.CreatedAsc:
                    sortExpression += "created+asc";
                    break;
                case IssuesOrder.CreatedDesc:
                    sortExpression += "created+desc";
                    break;
                case IssuesOrder.SummaryAsc:
                    sortExpression += "summary+asc";
                    break;
                case IssuesOrder.SummaryDesc:
                    sortExpression += "summary+desc";
                    break;
                case IssuesOrder.DescriptionAsc:
                    sortExpression += "description+asc";
                    break;
                case IssuesOrder.DescriptionDesc:
                    sortExpression += "description+desc";
                    break;
            }

            filter = string.IsNullOrWhiteSpace(filter)
                ? sortExpression
                : string.Format("{0}+{1}", filter, sortExpression);

            filter = filter.TrimStart('+');

            var feedbackIssues = _issueTracker.GetIssues(pager.PageSize, pager.GetStartIndex(), filter,
                new[] { "state", "description", "created", "summary" });
            var pagerShape = Shape.Pager(pager).TotalItemCount(_issueTracker.GetIssuesCount(filter));

            var model = new IssuesIndexViewModel {
                FeedbackIssues = feedbackIssues.Issues.Select(x => new FeedbackIssueEntry { FeedbackIssue = x }).ToList(),
                Options = options,
                Pager = pagerShape
            };

            // maintain previous route data when generating page links
            var routeData = new RouteData();
            routeData.Values.Add("Options.Filter", options.Filter);
            routeData.Values.Add("Options.Search", options.Search);
            routeData.Values.Add("Options.Order", options.Order);

            pagerShape.RouteData(routeData);

            return View(model);
        }
    }
}