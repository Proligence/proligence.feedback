﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Orchard;
using Orchard.Email.Services;
using Orchard.Localization;
using Orchard.Messaging.Services;
using Orchard.Mvc.Extensions;
using Orchard.Themes;
using Orchard.UI.Notify;
using Proligence.Feedback.Models;
using Proligence.Feedback.Services;
using Proligence.Feedback.ViewModels;

namespace Proligence.Feedback.Controllers {
    public class FeedbackController : Controller {
        private readonly IOrchardServices _orchardServices;
        private readonly IIssueTracker _issueTracker;
        private readonly IMessageService _messageService;

        public Localizer T { get; set; }

        public FeedbackController(IOrchardServices orchardServices, IIssueTracker issueTracker, IMessageService messageService) {
            _orchardServices = orchardServices;
            _issueTracker = issueTracker;
            _messageService = messageService;
            T = NullLocalizer.Instance;
        }

        [Themed]
        public ActionResult Index(string returnUrl) {
            var model = new IssueViewModel {
                ReturnUrl = returnUrl
            };

            return View(model);
        }

        [HttpPost]
        public ActionResult SendFeedback(IssueViewModel model) {
            var user = _orchardServices.WorkContext.CurrentUser;
            if (user != null) {
                try {
                    var result = _issueTracker.CreateIssue(
                        new IssueRequest {
                            Description = model.Description,
                            Summary = model.Summary,
                            Project = model.Project
                        });

                    var parameters = new Dictionary<string, object> {
                        { "Subject", T("Issue report confirmation").ToString() },
                        { "Body", T("Feedback sent, thank you. Your report has been granted an ID <b>{0}</b>.", result.IssueId).ToString() },
                        { "Recipients", _orchardServices.WorkContext.CurrentUser.Email },
                    };

                    _messageService.Send(SmtpMessageChannel.MessageType, parameters);

                    _orchardServices.Notifier.Add(
                        NotifyType.Information,
                        T("Feedback sent, thank you. Check you issues status <a href=\"{0}\">here</a>.", result.IssueUrl));
                }
                catch (InvalidOperationException e) {
                    _orchardServices.Notifier.Add(NotifyType.Error, T("Unable to send feedback. {0}", e.Message));
                }
            }

            return this.RedirectLocal(model.ReturnUrl, "~/");
        }
    }
}