﻿using System.Web.Mvc;
using Orchard;
using Orchard.Localization;
using Orchard.Logging;
using Orchard.Themes;
using Orchard.UI.Admin;
using Proligence.Feedback.Models;
using Proligence.Feedback.Services;

namespace Proligence.Feedback.Controllers {
    [Admin]
    public class TestAdminController : Controller {
        private readonly IOrchardServices _services;
        private readonly IIssueTracker _issueTracker;

        public TestAdminController(IOrchardServices services, IIssueTracker issueTracker) {
            _issueTracker = issueTracker;
            _services = services;
            T = NullLocalizer.Instance;
        }

        public ILogger Logger { get; set; }
        public Localizer T { get; set; }

        [Themed]
        public ActionResult Index() {
            if (!_services.Authorizer.Authorize(Permissions.ManageFeedback, T("Not authorized to test issues."))) {
                return new HttpUnauthorizedResult();
            }

            return View();
        }

        [Themed, HttpPost]
        public ActionResult Index(string description, string summary, bool isException) {
            if (!_services.Authorizer.Authorize(Permissions.ManageFeedback, T("Not authorized to test issues."))) {
                return new HttpUnauthorizedResult();
            }

            var issue = _issueTracker.CreateIssue(
                new IssueRequest {
                    Description = description,
                    IsException = isException,
                    Summary = summary
                });

            object result = string.Format("Status: {0}. Url: {1}", issue.Success, issue.IssueUrl);
            return View(result);
        }
    }
}