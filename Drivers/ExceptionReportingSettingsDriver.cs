﻿using Orchard.ContentManagement;
using Orchard.ContentManagement.Drivers;
using Orchard.Environment.Extensions;
using Proligence.Feedback.Models;

namespace Proligence.Feedback.Drivers {
    [OrchardFeature("Proligence.Feedback.ExceptionReporting")]
    public class ExceptionReportingSettingsDriver : ContentPartDriver<ExceptionReportingSettingsPart> {
        protected override DriverResult Editor(ExceptionReportingSettingsPart part, dynamic shapeHelper) {
            return ContentShape("Parts_ExceptionReporting_Settings",
                () => shapeHelper.EditorTemplate(TemplateName: "Parts.ExceptionReporting.Settings", Model: part, Prefix: Prefix))
                    .OnGroup("feedback");
        }

        protected override DriverResult Editor(ExceptionReportingSettingsPart part, IUpdateModel updater, dynamic shapeHelper) {
            updater.TryUpdateModel(part, Prefix, null, null);
            return Editor(part, shapeHelper);
        }
    }
}