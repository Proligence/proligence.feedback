﻿using Orchard.ContentManagement;
using Orchard.ContentManagement.Drivers;
using Proligence.Feedback.Models;

namespace Proligence.Feedback.Drivers {
    public class FeedbackWidgetPartDriver : ContentPartDriver<FeedbackWidgetPart> {
        protected override DriverResult Display(FeedbackWidgetPart part, string displayType, dynamic shapeHelper) {
            return ContentShape("Parts_Feedback_Widget",
                () => shapeHelper.Parts_Feedback_Widget(ContentItem: part.ContentItem, ContentPart: part));
        }

        protected override DriverResult Editor(FeedbackWidgetPart part, dynamic shapeHelper) {
            return ContentShape("Parts_Feedback_Widget",
                () => shapeHelper.EditorTemplate(TemplateName: "Parts.Feedback.Widget", Model: part, Prefix: Prefix));
        }

        protected override DriverResult Editor(FeedbackWidgetPart part, IUpdateModel updater, dynamic shapeHelper) {
            updater.TryUpdateModel(part, Prefix, null, null);
            return Editor(part, shapeHelper);
        }
    }
}