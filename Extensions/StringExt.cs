﻿using System;

namespace Proligence.Feedback.Extensions {
    public static class StringExt {
        public static string TruncateAtWord(this string input, int length) {
            if (input == null || input.Length < length - 4) {
                return input;
            }

            int iNextSpace = input.LastIndexOf(" ", length - 4, StringComparison.Ordinal);
            return string.Format("{0}...", input.Substring(0, (iNextSpace > 0) ? iNextSpace : length - 3).Trim());
        }
    }
}