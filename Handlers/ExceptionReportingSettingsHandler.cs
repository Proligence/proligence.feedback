﻿using Orchard.Caching.Services;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Handlers;
using Orchard.Environment.Extensions;
using Orchard.Localization;
using Proligence.Feedback.Models;
using Proligence.Feedback.Services;

namespace Proligence.Feedback.Handlers {
    [OrchardFeature("Proligence.Feedback.ExceptionReporting")]
    public class ExceptionReportingSettingsHandler : ContentHandler {
        public ExceptionReportingSettingsHandler(ICacheService cache) {
            Filters.Add(new ActivatingFilter<ExceptionReportingSettingsPart>("Site"));
            OnLoaded<ExceptionReportingSettingsPart>((ctx, settings) => {
                cache.Put(IssueTrackerExceptionPolicy.ReportExceptionsCacheKey, settings.ReportExceptions);
            });
        }

        public Localizer T { get; set; }

        protected override void GetItemMetadata(GetContentItemMetadataContext context) {
            if (context.ContentItem.ContentType == "Site") {
                base.GetItemMetadata(context);
                context.Metadata.EditorGroupInfo.Add(new GroupInfo(T("Feedback")));
            }
        }
    }
}