﻿using Orchard.ContentManagement.MetaData;
using Orchard.Data.Migration;

namespace Proligence.Feedback {
    public class WidgetsDataMigration : DataMigrationImpl  {
        public int Create()  {
            ContentDefinitionManager.AlterTypeDefinition("FeedbackWidget",
                cfg => cfg
                    .WithPart("WidgetPart")
                    .WithPart("FeedbackWidgetPart")
                    .WithPart("CommonPart")
                    .WithPart("IdentityPart")
                    .WithSetting("Stereotype", "Widget"));

            return 1;
        }
    }
}