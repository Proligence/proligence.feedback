﻿using Newtonsoft.Json;
using RestSharp.Deserializers;

namespace Proligence.Feedback.Models {
    public class Comment {
        [DeserializeAs(Name = "id")]
        [JsonProperty("id")]
        public string Id { get; set; }

        [DeserializeAs(Name = "author")]
        [JsonProperty("author")]
        public string Author { get; set; }

        [DeserializeAs(Name = "authorFullName")]
        [JsonProperty("authorFullName")]
        public string AuthorFullName { get; set; }

        [DeserializeAs(Name = "issueId")]
        [JsonProperty("issueId")]
        public string IssueId { get; set; }

        [DeserializeAs(Name = "parentId")]
        [JsonProperty("parentId")]
        public string ParentId { get; set; }

        [DeserializeAs(Name = "deleted")]
        [JsonProperty("deleted")]
        public bool Deleted { get; set; }

        [DeserializeAs(Name = "jiraId")]
        [JsonProperty("jiraId")]
        public string JiraId { get; set; }

        [DeserializeAs(Name = "text")]
        [JsonProperty("text")]
        public string Text { get; set; }

        [DeserializeAs(Name = "shownForIssueAuthor")]
        [JsonProperty("shownForIssueAuthor")]
        public bool ShownForIssueAuthor { get; set; }

        [DeserializeAs(Name = "created")]
        [JsonProperty("created")]
        public long Created { get; set; }

        [DeserializeAs(Name = "updated")]
        [JsonProperty("updated")]
        public long? Updated { get; set; }

        [DeserializeAs(Name = "idpermittedGroup")]
        [JsonProperty("idpermittedGroup")]
        public string PermittedGroup { get; set; }

        [DeserializeAs(Name = "replies")]
        [JsonProperty("replies")]
        public string[] Replies { get; set; }
    }
}