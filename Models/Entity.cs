﻿using Newtonsoft.Json;
using RestSharp.Deserializers;

namespace Proligence.Feedback.Models {
    public class Entity {
        [DeserializeAs(Name = "value")]
        [JsonProperty("value")]
        public int Value { get; set; }
    }
}