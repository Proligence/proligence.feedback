﻿using Orchard.ContentManagement;

namespace Proligence.Feedback.Models {
    public class ExceptionReportingSettingsPart : ContentPart {
        /// <summary>
        /// Enables/disables automatic exception logging.
        /// </summary>
        public bool ReportExceptions {
            get { return this.Retrieve(x => x.ReportExceptions); }
            set { this.Store(x => x.ReportExceptions, value); }
        }

        /// <summary>
        /// The name of the project in the issue tracking system to which exceptions will be reported.
        /// </summary>
        public string Project {
            get { return this.Retrieve(x => x.Project); }
            set { this.Store(x => x.Project, value); }
        }
    }
}