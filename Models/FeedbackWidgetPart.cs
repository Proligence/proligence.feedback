﻿using Orchard.ContentManagement;

namespace Proligence.Feedback.Models {
    public class FeedbackWidgetPart : ContentPart {
        public string Project
        {
            get { return this.Retrieve(x => x.Project); }
            set { this.Store(x => x.Project, value); }
        }
    }
}