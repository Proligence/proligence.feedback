﻿using Newtonsoft.Json;
using RestSharp.Deserializers;

namespace Proligence.Feedback.Models {
    public class Field {
        [DeserializeAs(Name = "name")]
        [JsonProperty("name")]
        public string Name { get; set; }

        [DeserializeAs(Name = "value")]
        [JsonProperty("value")]
        public object Value { get; set; }
    }
}