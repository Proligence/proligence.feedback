﻿namespace Proligence.Feedback.Models {
    /// <summary>
    /// Represents feedback request
    /// </summary>
    public interface IIssueRequest {
        /// <summary>
        /// Short summary for the feedback
        /// </summary>
        string Summary { get; }

        /// <summary>
        /// Description for the feedback
        /// </summary>
        string Description { get; }

        /// <summary>
        /// Feedback project
        /// </summary>
        string Project { get; set; }

        /// <summary>
        /// Determinates if issue is an exception
        /// </summary>
        bool IsException { get; set; }

        /// <summary>
        /// Current user
        /// </summary>
        string UserName { get; set; }
    }
}