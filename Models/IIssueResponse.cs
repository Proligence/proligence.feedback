﻿namespace Proligence.Feedback.Models {
    /// <summary>
    /// Represents feedback response
    /// </summary>
    public interface IIssueResponse {
        /// <summary>
        /// Is feedback success
        /// </summary>
        bool Success { get; }

        /// <summary>
        /// The issues URL
        /// </summary>
        string IssueUrl { get; }

        string IssueId { get; set; }
    }
}