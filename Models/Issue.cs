using System;
using System.Linq;
using Newtonsoft.Json;
using RestSharp.Deserializers;

namespace Proligence.Feedback.Models {
    public class Issue {
        [DeserializeAs(Name = "id")]
        [JsonProperty("id")]
        public string Id { get; set; }

        [DeserializeAs(Name = "jiraId")]
        [JsonProperty("jiraId")]
        public string JiraId { get; set; }

        [DeserializeAs(Name = "field")]
        [JsonProperty("field")]
        public Field[] Fields { get; set; }

        [DeserializeAs(Name = "comment")]
        [JsonProperty("comment")]
        public Comment[] Comments { get; set; }

        [DeserializeAs(Name = "tag")]
        [JsonProperty("tag")]
        public Tag[] Tags { get; set; }

        public string Summary {
            get {
                var singleOrDefault = Fields.SingleOrDefault(f => f.Name == "summary");
                return singleOrDefault != null ? singleOrDefault.Value.ToString() : string.Empty;
            }
        }

        public string Description {
            get {
                var singleOrDefault = Fields.SingleOrDefault(f => f.Name == "description");
                return singleOrDefault != null ? singleOrDefault.Value.ToString() : string.Empty;
            }
        }

        public string Created {
            get {
                var singleOrDefault = Fields.SingleOrDefault(f => f.Name == "created");
                return singleOrDefault != null
                    ? JavaTimeStampToDateTime(double.Parse(singleOrDefault.Value.ToString())).ToString()
                    : string.Empty;
            }
        }

        public string State {
            get {
                var singleOrDefault = Fields.SingleOrDefault(f => f.Name == "State");
                if (singleOrDefault != null) {
                    var list = (Newtonsoft.Json.Linq.JArray)singleOrDefault.Value;
                    return list[0].ToString();
                }

                return string.Empty;
            }
        }

        private static DateTime JavaTimeStampToDateTime(double javaTimeStamp) {
            // Java timestamp is millisecods past epoch
            var dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddMilliseconds(Math.Round(javaTimeStamp)).ToLocalTime();
            return dtDateTime;
        }
    }
}