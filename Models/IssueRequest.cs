﻿namespace Proligence.Feedback.Models {
    /// <summary>
    /// Represents feedback request
    /// </summary>
    public class IssueRequest : IIssueRequest {
        /// <summary>
        /// Short summary for the feedback
        /// </summary>
        public string Summary { get; set; }

        /// <summary>
        /// Description for the feedback
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Feedback project
        /// </summary>
        public string Project { get; set; }
        
        /// <summary>
        /// Determinates if issue is an exception
        /// </summary>
        public bool IsException { get; set; }

        public string UserName { get; set; }
    }
}