﻿namespace Proligence.Feedback.Models {
    /// <summary>
    /// Represents feedback response
    /// </summary>
    public class IssueResponse : IIssueResponse {
        /// <summary>
        /// Is feedback success
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// The issues URL
        /// </summary>
        public string IssueUrl { get; set; }

        public string IssueId { get; set; }
    }
}