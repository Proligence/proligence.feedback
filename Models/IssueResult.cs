﻿using Newtonsoft.Json;
using RestSharp.Deserializers;

namespace Proligence.Feedback.Models {
    public class IssueResult {
        [DeserializeAs(Name = "issue")]
        [JsonProperty("issue")]
        public Issue[] Issues { get; set; }
    }
}