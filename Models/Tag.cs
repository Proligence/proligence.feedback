﻿using Newtonsoft.Json;
using RestSharp.Deserializers;

namespace Proligence.Feedback.Models {
    public class Tag {
        [DeserializeAs(Name = "value")]
        [JsonProperty("value")]
        public string Value { get; set; }
    }
}