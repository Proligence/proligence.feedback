﻿using Newtonsoft.Json;
using RestSharp.Deserializers;

namespace Proligence.Feedback.Models {
    public class YoutrackRawType {
        [DeserializeAs(Name = "rawType")]
        [JsonProperty("rawType")]
        public string RawType { get; set; }

        [DeserializeAs(Name = "type")]
        [JsonProperty("type")]
        public string Type { get; set; }

        [DeserializeAs(Name = "entity")]
        [JsonProperty("entity")]
        public Entity Entity { get; set; }
    }
}