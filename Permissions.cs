﻿using System.Collections.Generic;
using Orchard.Environment.Extensions.Models;
using Orchard.Security.Permissions;

namespace Proligence.Feedback {
    public class Permissions : IPermissionProvider {
        public static readonly Permission ManageFeedback = new Permission {
            Name = "ManageFeedback",
            Description = "Managing Feedback"
        };

        public virtual Feature Feature { get; set; }

        public IEnumerable<Permission> GetPermissions() {
            yield return ManageFeedback;
        }

        public IEnumerable<PermissionStereotype> GetDefaultStereotypes() {
            yield return new PermissionStereotype {
                Name = "Administrator",
                Permissions = new[] { ManageFeedback }
            };
        }
    }
}