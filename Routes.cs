﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Web.Mvc;
using System.Web.Routing;
using Orchard.Mvc.Routes;

namespace Proligence.Feedback {
    [ExcludeFromCodeCoverage]
    public class Routes : IRouteProvider {
        public void GetRoutes(ICollection<RouteDescriptor> routes) {
            foreach (RouteDescriptor routeDescriptor in GetRoutes()) {
                routes.Add(routeDescriptor);
            }
        }

        public IEnumerable<RouteDescriptor> GetRoutes() {
            return new[] {
                new RouteDescriptor {
                    Priority = -5,
                    Route = new Route(
                        "Admin/Feedback/Test",
                        new RouteValueDictionary  {
                            { "area", "Proligence.Feedback" },
                            { "controller", "TestAdmin" },
                            { "action", "Index" }
                        },
                    new RouteValueDictionary(),
                    new RouteValueDictionary { {"area", "Proligence.Feedback"} },
                    new MvcRouteHandler())
                },
                new RouteDescriptor {
                    Priority = -5,
                    Route = new Route(
                        "Feedback",
                        new RouteValueDictionary  {
                            { "area", "Proligence.Feedback" },
                            { "controller", "Feedback" },
                            { "action", "Index" }
                        },
                    new RouteValueDictionary(),
                    new RouteValueDictionary { {"area", "Proligence.Feedback"} },
                    new MvcRouteHandler())
                },
                new RouteDescriptor
                {
                    Priority = -5,
                    Route = new Route(
                        "Admin/Feedback",
                        new RouteValueDictionary  {
                            { "area", "Proligence.Feedback" },
                            { "controller", "Admin" },
                            { "action", "Index" }
                        },
                    new RouteValueDictionary(),
                    new RouteValueDictionary { {"area", "Proligence.Feedback"} },
                    new MvcRouteHandler())
                }
            };
        }
    }
}