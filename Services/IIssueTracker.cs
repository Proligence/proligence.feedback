﻿using Orchard;
using Proligence.Feedback.Models;

namespace Proligence.Feedback.Services {
    public interface IIssueTracker : IDependency {
        IIssueResponse CreateIssue(IIssueRequest request);
        IssueResult GetIssues(int max = 10, int after = 0, string filter = null, string[] withFields = null);
        int GetIssuesCount(string filter = null);
    }
}