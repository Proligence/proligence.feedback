﻿using Orchard.Localization;
using Orchard.UI.Notify;

namespace Proligence.Feedback.Services {
    public class IssueNotifier : Notifier, INotifier {
        void INotifier.Add(NotifyType type, LocalizedString message) {
            // TODO: Here will come the appending of "Do you want to report this" text to all errors.
            base.Add(type, message);
        }
    }
}