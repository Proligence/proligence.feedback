﻿using System;
using Orchard;
using Orchard.Caching.Services;
using Orchard.Environment;
using Orchard.Environment.Extensions;
using Orchard.Exceptions;
using Orchard.Security;
using Orchard.UI.Notify;
using Proligence.Feedback.Models;

namespace Proligence.Feedback.Services {
    [OrchardFeature("Proligence.Feedback.ExceptionReporting")]
    [OrchardSuppressDependency("Orchard.Exceptions.DefaultExceptionPolicy")]
    public class IssueTrackerExceptionPolicy : DefaultExceptionPolicy, IExceptionPolicy {
        public const string ReportExceptionsCacheKey = "ReportExceptions";

        private readonly IIssueTracker _issueTracker;
        private readonly ICacheService _cache;
        private readonly IWorkContextAccessor _workContextAccessor;

        public IssueTrackerExceptionPolicy(
            ICacheService cache,
            INotifier notifier,
            Work<IAuthorizer> authorizer,
            IIssueTracker issueTracker,
            IWorkContextAccessor workContextAccessor)
            : base(notifier, authorizer) {

            _cache = cache;
            _workContextAccessor = workContextAccessor;
            _issueTracker = issueTracker;
        }

        bool IExceptionPolicy.HandleException(object sender, Exception exception) {
            var logExceptions = _cache.Get<bool>(ReportExceptionsCacheKey);
            var context = _workContextAccessor.GetContext();

            string userName = null;
            if (context != null && context.CurrentUser != null) {
                userName = context.CurrentUser.UserName;
            }

            // check for settings to log exception
            if (logExceptions) {
                var issue = new IssueRequest {
                    Description = exception.ToString(),
                    Summary = exception.Message,
                    IsException = true,
                    UserName = userName
                };

                _issueTracker.CreateIssue(issue);
            }

            return base.HandleException(sender, exception);
        }
    }
}