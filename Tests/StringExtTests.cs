﻿using Proligence.Feedback.Extensions;
using Xunit;

namespace Proligence.Feedback.Tests {
    public class StringExtTests {
        [Fact]
        public void TruncateAtWordShouldReturnProperLength() {
            var myString = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing";
            var newlength = 50;
            myString = myString.TruncateAtWord(newlength);
            Assert.True(myString.Length <= newlength);
        }

        [Fact]
        public void TruncateAtWordShouldReturnProperExactLength() {
            var myString = "Lorem Ipsum";
            var newlength = 10;
            myString = myString.TruncateAtWord(newlength);
            Assert.Equal(myString.Length, 8);
        }

        [Fact]
        public void TruncateAtWordShouldNotTrimShortStrings() {
            var myString = "Lorem";
            var newlength = 50;
            var newMyString = myString.TruncateAtWord(newlength);
            Assert.Equal(newMyString, myString);
        }

        [Fact]
        public void TruncateAtWordShouldTrimLongWord() {
            var myString = "Loremtypesetting";
            var newlength = 10;
            var newMyString = myString.TruncateAtWord(newlength);
            Assert.True(myString.Length > newMyString.Length);
        }

        [Fact]
        public void TruncateAtWordShouldNotTrimEmpty() {
            var myString = string.Empty;
            var newlength = 10;
            var newMyString = myString.TruncateAtWord(newlength);
            Assert.Equal(newMyString.Length, 0);
        }
    }
}