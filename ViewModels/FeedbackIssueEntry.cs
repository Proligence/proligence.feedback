using Proligence.Feedback.Models;

namespace Proligence.Feedback.ViewModels {
    public class FeedbackIssueEntry {
        public Issue FeedbackIssue { get; set; }
        public bool IsChecked { get; set; }
    }
}