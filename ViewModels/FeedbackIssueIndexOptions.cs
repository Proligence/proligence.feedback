﻿namespace Proligence.Feedback.ViewModels {
    public class FeedbackIssueIndexOptions {
        public string Search { get; set; }
        public IssuesOrder Order { get; set; }
        public IssuesFilter Filter { get; set; }
    }
}