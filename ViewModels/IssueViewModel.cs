﻿namespace Proligence.Feedback.ViewModels {
    public class IssueViewModel {
        public string ReturnUrl { get; set; }
        public string Project { get; set; }
        public string Summary { get; set; }
        public string Description { get; set; }
    }
}