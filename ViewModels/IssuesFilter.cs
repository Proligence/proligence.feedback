﻿namespace Proligence.Feedback.ViewModels {
    public enum IssuesFilter {
        All,
        New,
        Open,
        ToBeDiscussed,
        InProgress,
        CantReproduce,
        Duplicate,
        WontFix,
        Obsolete,
        Closed,
        WaitingVerification
    }
}