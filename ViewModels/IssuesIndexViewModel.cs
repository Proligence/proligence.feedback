﻿using System.Collections.Generic;

namespace Proligence.Feedback.ViewModels  {
    public class IssuesIndexViewModel {
        public IList<FeedbackIssueEntry> FeedbackIssues { get; set; }
        public FeedbackIssueIndexOptions Options { get; set; }
        public dynamic Pager { get; set; }

        public IssuesIndexViewModel() {
            FeedbackIssues = new List<FeedbackIssueEntry>();
        }
    }
}