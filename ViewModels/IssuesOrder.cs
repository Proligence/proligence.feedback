namespace Proligence.Feedback.ViewModels {
    public enum IssuesOrder {
        IdAsc,
        IdDesc,
        StateAsc,
        StateDesc,
        CreatedAsc,
        CreatedDesc,
        SummaryAsc,
        SummaryDesc,
        DescriptionAsc,
        DescriptionDesc
    }
}