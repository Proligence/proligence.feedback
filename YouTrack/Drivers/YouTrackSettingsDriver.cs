﻿using Orchard.ContentManagement;
using Orchard.ContentManagement.Drivers;
using Orchard.Environment.Extensions;
using Proligence.Feedback.YouTrack.Models;

namespace Proligence.Feedback.YouTrack.Drivers {
    [OrchardFeature("Proligence.Feedback.YouTrack")]
    public class YouTrackSettingsDriver : ContentPartDriver<YouTrackSettingsPart> {
        protected override DriverResult Editor(YouTrackSettingsPart part, dynamic shapeHelper) {
            return ContentShape("Parts_YouTrack_Settings", 
                () => shapeHelper.EditorTemplate(TemplateName: "Parts.YouTrack.Settings", Model: part, Prefix: Prefix))
                    .OnGroup("feedback");
        }

        protected override DriverResult Editor(YouTrackSettingsPart part, IUpdateModel updater, dynamic shapeHelper) {
            updater.TryUpdateModel(part, Prefix, null, null);

            // Just in case: Remove the last slash  
            if (string.IsNullOrWhiteSpace(part.Url) == false) {
                part.Url = part.Url.TrimEnd('/');
            }

            return Editor(part, shapeHelper);
        }
    }
}