﻿using Orchard.ContentManagement.Handlers;
using Orchard.Environment.Extensions;
using Proligence.Feedback.YouTrack.Models;

namespace Proligence.Feedback.YouTrack.Handlers {
    [OrchardFeature("Proligence.Feedback.YouTrack")]
    public class YouTrackSettingsHandler : ContentHandler {
        public YouTrackSettingsHandler()  {
            Filters.Add(new ActivatingFilter<YouTrackSettingsPart>("Site"));
        }
    }
}