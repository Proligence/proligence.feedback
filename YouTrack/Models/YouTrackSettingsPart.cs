﻿using Orchard.ContentManagement;
using Proligence.Feedback.YouTrack.Services;

namespace Proligence.Feedback.YouTrack.Models {    
    /// <summary>
    /// The content part model. Implements <see cref="IYouTrackIssueServiceParameters"/>
    /// </summary>
    public class YouTrackSettingsPart : ContentPart, IYouTrackIssueServiceParameters {
        public int RestTimeout {
            get { return this.Retrieve(x => x.RestTimeout); }
            set { this.Store(x => x.RestTimeout, value); }
        }

        /// <summary>
        /// URL to YouTrack. e.g.: https://proligence.myjetbrains.com/youtrack/
        /// </summary>
        public string Url {
            get { return this.Retrieve(x => x.Url); }
            set { this.Store(x => x.Url, value); }
        }

        /// <summary>
        /// YouTrack Login 
        /// </summary>
        public string UserName {
            get { return this.Retrieve(x => x.UserName); }
            set { this.Store(x => x.UserName, value); }
        }

        /// <summary>
        /// YouTrack Password
        /// </summary>
        public string Password {
            get { return this.Retrieve(x => x.Password); }
            set { this.Store(x => x.Password, value); }
        }

        /// <summary>
        /// Default project to report feedback and exceptions
        /// </summary>
        public string DefaultProject {
            get { return this.Retrieve(x => x.DefaultProject); }
            set { this.Store(x => x.DefaultProject, value); }
        }

        public bool IsValid() {
            if (string.IsNullOrWhiteSpace(UserName) || string.IsNullOrWhiteSpace(Password) || string.IsNullOrWhiteSpace(Url)) {
                return false;
            }

            return true;
        }
    }
}