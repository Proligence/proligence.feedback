﻿using Orchard;

namespace Proligence.Feedback.YouTrack.Services {
    /// <summary>
    /// Represents the configuration of the YouTrack service
    /// </summary>
    public interface IYouTrackIssueServiceParameters : IDependency {
        /// <summary>
        /// Gets the timeout for REST web service calls (in seconds).
        /// </summary>
        int RestTimeout { get; }

        /// <summary>
        /// Gets the URL to the YouTrack instance to which issues will be reported.
        /// </summary>
        string Url { get; }

        /// <summary>
        /// Gets the user name which will be used to log into YouTrack.
        /// </summary>
        string UserName { get; }

        /// <summary>
        /// Gets the password name which will be used to log into YouTrack.
        /// </summary>
        string Password { get; }

        /// <summary>
        /// Gets the ID of the YouTrack project to which issues will be reported.
        /// </summary>
        string DefaultProject { get; }
    }
}