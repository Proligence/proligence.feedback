﻿using System.Collections.Generic;
using System.Web.Mvc;
using Orchard;
using Orchard.ContentManagement;
using Orchard.Environment.Extensions;
using Orchard.Localization;
using Orchard.UI.Admin.Notification;
using Orchard.UI.Notify;
using Proligence.Feedback.YouTrack.Models;

namespace Proligence.Feedback.YouTrack.Services  {
    [OrchardFeature("Proligence.Feedback.YouTrack")]
    public class MissingSettingsBanner : INotificationProvider {
        private readonly IOrchardServices _orchardServices;

        public Localizer T { get; set; }

        public MissingSettingsBanner(IOrchardServices orchardServices) {
            _orchardServices = orchardServices;
            T = NullLocalizer.Instance;
        }

        public IEnumerable<NotifyEntry> GetNotifications() {
            var workContext = _orchardServices.WorkContext;
            var youtrackSettings = workContext.CurrentSite.As<YouTrackSettingsPart>();
            if (youtrackSettings == null || !youtrackSettings.IsValid()) {
                var urlHelper = new UrlHelper(workContext.HttpContext.Request.RequestContext);
                var url = urlHelper.Action("Index", "Admin", new { Area = "Settings" });
                yield return new NotifyEntry {
                    Type = NotifyType.Warning,
                    Message = T("The <a href=\"{0}\">YouTrack settings</a> need to be configured.", url)
                };
            }
        }
    }
}