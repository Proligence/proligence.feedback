﻿namespace Proligence.Feedback.YouTrack.Services {
    /// <summary>
    /// Represents the configuration of the YouTrack service
    /// </summary>
    public class YouTrackIssueServiceParameters : IYouTrackIssueServiceParameters {
        /// <summary>
        /// Gets the timeout for REST web service calls (in seconds).
        /// </summary>
        public int RestTimeout { get; set; }

        /// <summary>
        /// Gets the URL to the YouTrack instance to which issues will be reported.
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// Gets the user name which will be used to log into YouTrack.
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Gets the password name which will be used to log into YouTrack.
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Gets the ID of the YouTrack project to which issues will be reported.
        /// </summary>
        public string DefaultProject { get; set; }

        /// <summary>
        /// Gets the ID of the YouTrack project to  which exceptions will be reported.
        /// </summary>
        public string ExceptionProject { get; set; }
    }
}