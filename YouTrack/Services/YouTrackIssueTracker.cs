﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using Newtonsoft.Json;
using Orchard.ContentManagement;
using Orchard.Environment.Extensions;
using Orchard.Localization;
using Orchard.Settings;
using Orchard.UI.Notify;
using Proligence.Feedback.Models;
using Proligence.Feedback.Services;
using Proligence.Feedback.YouTrack.Models;
using RestSharp;

namespace Proligence.Feedback.YouTrack.Services {
    [OrchardFeature("Proligence.Feedback.YouTrack")]
    public class YouTrackIssueTracker : IIssueTracker {
        private readonly ISiteService _siteService;
        private readonly INotifier _notifier;
        private readonly IRestClient _restClient;

        public Localizer T { get; set; }

        /// <summary>
        /// Determines whether the REST client has been logged into YouTrack.
        /// </summary>
        private bool _loggedIn;

        public YouTrackIssueTracker(ISiteService siteService, INotifier notifier) {
            _siteService = siteService;
            _notifier = notifier;
            _restClient = new RestClient { CookieContainer = new CookieContainer() };
            T = NullLocalizer.Instance;
        }

        public IIssueResponse CreateIssue(IIssueRequest request) {
            if (request == null) {
                throw new ArgumentNullException("request");
            }

            var config = _siteService.GetSiteSettings().As<YouTrackSettingsPart>();
            LogIntoYouTrack(config);

            if (string.IsNullOrWhiteSpace(request.Project)) {
                request.Project = config.DefaultProject;
            }

            return TryHandleExistingIssue(request, config) ?? HandleNewIssue(request, config);
        }

        /// <summary>
        /// Handle New issue, create and validare response
        /// </summary>
        private IIssueResponse HandleNewIssue(IIssueRequest request, IYouTrackIssueServiceParameters config) {
            var restRequest = CreateIssueRequest(request, config);
            var response = _restClient.Execute(restRequest);
            ValidateResponse(response, HttpStatusCode.Created);

            var issueResponse = new IssueResponse {
                Success = true,
                IssueUrl = response.Headers.Single(row => row.Name == "Location").Value.ToString().Replace("rest/", string.Empty)
            };

            issueResponse.IssueId = issueResponse.IssueUrl.Split('/').Last();

            if (request.IsException) {
                var issueName = issueResponse.IssueUrl.Split('/').Last();
                var hash = ComputeHash(request);
                var fields = new Dictionary<string, string> { { "command", CustomFields.Hash + " " + hash } };
                var updateRequest = CreateIssueUpdateRequest(config, issueName, fields);
                var updateRespone = _restClient.Execute(updateRequest);
                ValidateResponse(updateRespone, HttpStatusCode.OK);
            }

            return issueResponse;
        }

        /// <summary>
        /// Handle existing issue, add comment and repon if is closed 
        /// </summary>
        private IIssueResponse TryHandleExistingIssue(IIssueRequest request, IYouTrackIssueServiceParameters config) {
            if (!request.IsException) {
                return null;
            }

            var issue = GetIssueWithHash(request, config);
            if (issue == null) {
                return null;
            }

            var commandFields = new Dictionary<string, string>();
            var comment = string.IsNullOrEmpty(request.UserName)
                ? T("The exception occurred again").Text
                : T("The exception occurred again. User: {0}", request.UserName).Text;

            commandFields.Add("comment", comment);

            var statuses = new List<string> { "Closed", "Can't Reproduce" };
            if (statuses.Contains(issue.State, StringComparer.OrdinalIgnoreCase)) {
                commandFields.Add("command", "State Open");
            }

            var updateRequest = CreateIssueUpdateRequest(config, issue.Id, commandFields);
            var updateRespone = _restClient.Execute(updateRequest);
            ValidateResponse(updateRespone, HttpStatusCode.OK);

            // Build URL based on issue found in YouTrack
            var url = string.Format("{0}/issue/{1}", config.Url.TrimEnd('/'), issue.Id);
            return new IssueResponse { Success = true, IssueUrl = url };
        }

        private string GetProjectName(IIssueRequest request, IYouTrackIssueServiceParameters config) {
            var projectName = config.DefaultProject;
            var exceptionReporting = _siteService.GetSiteSettings().As<ExceptionReportingSettingsPart>();

            if (!string.IsNullOrWhiteSpace(request.Project)) {
                projectName = request.Project;
            }
            else if (request.IsException) {
                if (exceptionReporting != null && exceptionReporting.ReportExceptions && !string.IsNullOrWhiteSpace(exceptionReporting.Project)) {
                    projectName = exceptionReporting.Project;
                }
            }

            return projectName;
        }

        /// <summary>
        /// Check if issue exists by hash
        /// </summary>
        private Issue GetIssueWithHash(IIssueRequest request, IYouTrackIssueServiceParameters config) {
            var project = GetProjectName(request, config);
            var hash = ComputeHash(request);
            var filter = string.Format("{0}:{1} {2}:{3}", "Project", project, CustomFields.Hash, hash);
            var issue = GetIssues(filter: filter).Issues.FirstOrDefault();

            return issue;
        }

        /// <summary>
        /// Creates MD5 Hash
        /// </summary>
        /// <returns>computed MD5 Hash</returns>
        private string ComputeHash(IIssueRequest request) {
            var text = request.Summary + request.Description;

            string result;
            using (var ms = new MemoryStream(Encoding.UTF8.GetBytes(text))) {
                result = string.Join("", new MD5CryptoServiceProvider().ComputeHash(ms).Select(x => x.ToString("X2"))).ToLowerInvariant();
            }

            return result;
        }

        private RestRequest CreateIssueRequest(IIssueRequest request, IYouTrackIssueServiceParameters config) {
            var restRequest = CreateRequest("/rest/issue", Method.PUT, config);
            var project = GetProjectName(request, config);

            restRequest.AddParameter("project", project);
            restRequest.AddParameter("summary", request.Summary);
            restRequest.AddParameter("description", request.Description);

            return restRequest;
        }

        /// <summary>
        /// According to the Youtrack documentation, is not possible to create issue and 
        /// set custom fields in one request, we need to use command to update issue with some data
        /// </summary>
        /// <param name="issueName">Name of the Issue</param>
        /// <param name="fields">Fiels like command, comment, group etc. Full info under : https://confluence.jetbrains.com/display/YTD6/Apply+Command+to+an+Issue </param>
        /// <returns></returns>
        private RestRequest CreateIssueUpdateRequest(IYouTrackIssueServiceParameters config, string issueName, Dictionary<string, string> fields) {
            var restRequest = CreateRequest(string.Format("/rest/issue/{0}/execute", issueName), Method.POST, config);
            foreach (var value in fields) {
                restRequest.AddParameter(value.Key, value.Value);
            }

            return restRequest;
        }

        /// <summary>
        /// Validates if response from Youtrack is with the expected code 
        /// </summary>
        public void ValidateResponse(IRestResponse response, HttpStatusCode expectedStatus) {
            if (response.StatusCode != expectedStatus) {
                throw new InvalidOperationException(
                    string.Format(
                        CultureInfo.CurrentCulture,
                        "REST response failed. Status: {0}, Description: {1}, Returned content: {2}",
                        response.StatusCode,
                        response.StatusDescription,
                        response.Content));
            }
        }

        public IssueResult GetIssues(int max = 10, int after = 0, string filter = null, string[] withFields = null) {
            // GET /rest/issue?{filter}&{with}&{max}&{after}

            var config = _siteService.GetSiteSettings().As<YouTrackSettingsPart>();
            if (config.IsValid() == false) {
                _notifier.Warning(T("Configuration is not valid. Please check it"));
                return new IssueResult();
            }

            // get only those issues reported by service user
            filter = "reported+by:+" + config.UserName + "+" + filter;

            LogIntoYouTrack(config);
            RestRequest restRequest = CreateRequest("/rest/issue", Method.GET, config);
            
            // this is a terrible hack, but otherwise - using only the AddParameter function - filter parameter is omitted 
            // even providing it as a ParameterType.UrlSegmnet
            if (!string.IsNullOrWhiteSpace(filter)) {
                restRequest.Resource += string.Format("?filter={0}&max={1}&after={2}", filter, max, after);
                if (withFields != null) {
                    foreach (var withField in withFields) {
                        restRequest.Resource += string.Format("&with={0}", withField);
                    }
                }
            }
            else {
                restRequest.AddParameter("max", max);
                restRequest.AddParameter("after", after);
                if (withFields != null) {
                    foreach (var withField in withFields) {
                        restRequest.AddParameter("with", withField);
                    }
                }
            }

            var response = _restClient.Execute<IssueResult>(restRequest);
            if (response.Data == null) {
                response.Data = JsonConvert.DeserializeObject<IssueResult>(response.Content);
            }

            if (response.StatusCode != HttpStatusCode.OK) {
                throw new InvalidOperationException(string.Format(
                    CultureInfo.CurrentCulture,
                    "REST response failed. Status: {0}, Description: {1}, Returned content: {2}",
                    response.StatusCode,
                    response.StatusDescription,
                    response.Content));
            }

            return response.Data;
        }

        public int GetIssuesCount(string filter = null) {
            // GET /rest/issue/count?{filter}&{callback}

            var config = _siteService.GetSiteSettings().As<YouTrackSettingsPart>();
            LogIntoYouTrack(config);

            RestRequest restRequest = CreateRequest("/rest/issue/count", Method.GET, config);

            // this is a terrible hack, but otherwise - using only the AddParameter function - filter parameter is omitted 
            // even providing it as a ParameterType.UrlSegmnet
            if (!string.IsNullOrWhiteSpace(filter)) {
                restRequest.Resource += "?filter=" + filter;
            }

            var response = _restClient.Execute<Entity>(restRequest);
            if (response.Data == null) {
                response.Data = JsonConvert.DeserializeObject<Entity>(response.Content);
            }

            if (response.StatusCode != HttpStatusCode.OK) {
                throw new InvalidOperationException(string.Format(
                    CultureInfo.CurrentCulture,
                    "REST response failed. Status: {0}, Description: {1}, Returned content: {2}",
                    response.StatusCode,
                    response.StatusDescription,
                    response.Content));
            }

            return response.Data.Value;

            // TODO: If you get '-1' in response instead of number of issues, it means that the response was sent before the count had been completed. Please try again in a moment.
        }

        /// <summary>
        /// Ensures that the REST client is logged into YouTrack.
        /// </summary>
        private void LogIntoYouTrack(IYouTrackIssueServiceParameters config) {
            if (!_loggedIn) {
                RestRequest request = CreateRequest("/rest/user/login", Method.POST, config);
                request.AddParameter("login", config.UserName);
                request.AddParameter("password", config.Password);

                ExecuteJsonRequest(request);
                
                _loggedIn = true;
            }
        }

        /// <summary>
        /// Creates a new REST request.
        /// </summary>
        /// <param name="url">The relative URL of the requested resource.</param>
        /// <param name="method">The request method to use.</param>
        /// <param name="config">Service configuration.</param>
        /// <returns>The created request.</returns>
        private RestRequest CreateRequest(string url, Method method, IYouTrackIssueServiceParameters config) {
            return new RestRequest(config.Url + url, method) {
                Timeout = config.RestTimeout * 1000
            };
        }

        /// <summary>
        /// Executes the specified response.
        /// </summary>
        /// <param name="request">The response to execute.</param>
        /// <returns>The executed response.</returns>
        public virtual IRestResponse ExecuteRequest(IRestRequest request) {
            if (request == null) {
                throw new ArgumentNullException("request");
            }

            return _restClient.Execute(request);
        }

        /// <summary>
        /// Posts the specified object serialized to JSON format.
        /// </summary>
        /// <param name="request">The request to execute.</param>
        public virtual void ExecuteJsonRequest(RestRequest request) {
            IRestResponse response = ExecuteRequest(request);
            ValidateResponse(response);
        }

        /// <summary>
        /// Validates the specified REST response.
        /// </summary>
        /// <param name="response">The response to validate.</param>
        protected virtual void ValidateResponse(IRestResponse response) {
            if (response == null) {
                throw new ArgumentNullException("response");
            }

            if (response.StatusCode != HttpStatusCode.OK) {
                throw new InvalidOperationException(
                    string.Format(
                    CultureInfo.CurrentCulture,
                    "REST response failed. Status: {0}, Description: {1}, Returned content: {2}",
                    response.StatusCode,
                    response.StatusDescription,
                    response.Content));
            }
        }
    }
}